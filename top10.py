#!/usr/bin/env python3
import sys
import os
import csv
import json
import urllib.parse
import urllib.request

with open(sys.argv[1], 'r') as csvfile:
    urlreader = csv.reader(csvfile)

    topten = []

    for row in urlreader:
        if len(topten) >= 10:
            break

        if len(row) < 1 or not row[0].startswith('/mwoskill/?'):
            continue

        url = urllib.parse.urlparse(row[0])
        if not url.query:
            continue
        qs = urllib.parse.parse_qs(url.query)
        if 'p' not in qs or not qs['p']:
            continue

        permlink = qs['p'][0]
        if permlink == 'null':
            # yeah, wtf did I do wrong on the site
            continue

        permlinkpath = os.path.join('json', permlink)
        if not os.path.isfile(permlinkpath) or os.path.getsize(permlinkpath) <= 0:
            continue

        topten.append(permlink);

    if topten:
        print(json.dumps(topten, indent=2))
