#!/usr/bin/env python3
import sys
import os
import csv
import json
import urllib.parse
import urllib.request

output = set()

for datafile in sys.argv[1:]:
    with open(datafile, 'r') as csvfile:
        urlreader = csv.reader(csvfile)

        for row in urlreader:
            if len(output) >= 1000:
                break

            if len(row) < 1 or not row[0].startswith('/mwoskill/?'):
                continue

            url = urllib.parse.urlparse(row[0])
            if not url.query:
                continue
            qs = urllib.parse.parse_qs(url.query)
            if 'p' not in qs or not qs['p']:
                continue

            permlink = qs['p'][0]
            if permlink == 'null':
                # yeah, wtf did I do wrong on the site
                continue

            permlinkpath = os.path.join('json', permlink)
            if not os.path.isfile(permlinkpath) or os.path.getsize(permlinkpath) <= 0:
                continue

            if permlink in output:
                continue

            output.add(permlink);
            print(permlink)
