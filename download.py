#!/usr/bin/env python3
import sys
import time
import random
import os
import csv
import urllib.parse
import urllib.request
import http.cookiejar

cj = http.cookiejar.CookieJar()

with open(sys.argv[1], 'r') as csvfile:
    urlreader = csv.reader(csvfile)
    for row in urlreader:
        if len(row) < 1 or not row[0].startswith('/mwoskill/?'):
            continue

        url = urllib.parse.urlparse(row[0])
        if not url.query:
            continue
        qs = urllib.parse.parse_qs(url.query)
        if 'p' not in qs or not qs['p']:
            continue

        permlink = qs['p'][0]
        if permlink == 'null':
            # yeah, wtf did I do wrong on the site
            continue

        permlinkpath = os.path.join('json', permlink)
        if os.path.isfile(permlinkpath) and os.path.getsize(permlinkpath) > 0:
            #print("[exists {}]".format(permlink))
            continue

        if permlink.startswith('jsonbin1.'):
            permlink = permlink[9:]
            geturl = "https://api.jsonbin.io/b/" + permlink
        else:
            geturl = "https://jsonblob.com/api/jsonBlob/" + permlink

        print("[downloading {}]".format(geturl))
        try:
            opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
            opener.addheaders = [
                    ('Accept', 'application/json'),
                    ('Content-Type', 'application/json')
            ]
            connect = opener.open(geturl, timeout=10)

            with open(permlinkpath, 'wb') as f:
                while True:
                    buffer = connect.read(4096)
                    if not buffer:
                        break
                    f.write(buffer)
            connect.close()
        except Exception as e:
            print(e)

        time.sleep(random.randint(2,5))
